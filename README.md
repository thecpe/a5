# LIS4368 - Advancced Web Applications Development

## Christian Pelaez-Espinosa

### Assignment 5#

> This assignment was focused on **Model View Controller (MVC)** which is a framework that divides an application's implementation into three
discrete component roles: models, views, and controllers.

* Model: - The “business” layer. how do we represent the data? Defines business objects, and provides
methods for business processing (e.g., Customer and CustomerDB classes).

* Controller: Directs the application “flow”—using servlets. Generally, reads parameters sent from
requests, updates the model, saves data to the data store, and forwards updated model to JSPs for
presentation (e.g., CustomerListServlet).

* View: How do I render the result? Defines the presentation—using .html or .jsp files (e.g., index.html,
index.jsp, thanks.jsp).

We conducted a form entry validation where the user inputed data and it was entered into our mysql database

#### Assignment #5 ScreenShots:

ScreenShot 1: *Valid User Form Entry* ![Screen Shot 2016-04-07 at 1.41.10 PM.png](https://bitbucket.org/repo/RzE65G/images/250615543-Screen%20Shot%202016-04-07%20at%201.41.10%20PM.png)

ScreenShot 2: *Passed Validation* ![Screen Shot 2016-04-07 at 1.40.59 PM.png](https://bitbucket.org/repo/RzE65G/images/2458517084-Screen%20Shot%202016-04-07%20at%201.40.59%20PM.png)

ScreenShot3: *Associated Database Entry* ![Screen Shot 2016-04-07 at 1.41.52 PM.png](https://bitbucket.org/repo/RzE65G/images/2433598622-Screen%20Shot%202016-04-07%20at%201.41.52%20PM.png)